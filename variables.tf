variable "additional_subnet_tags" {
  default     = {}
  description = "Additional tags to be set on subnets provisioned by this module. Typically used for subnet auto-discovery such as with an AWS load balancer controller on EKS."
  type        = map(string)
}

variable "availability_zone_ids" {
  default     = []
  description = "A list of zone IDs for AWS availability zones in which to provision subnets for this VPC's NAT gateway."
  type        = list(string)
}

variable "cidr_block" {
  default     = "10.0.0.0/16"
  description = "The root CIDR block for this VPC."
  type        = string
}

variable "domain" {
  default     = "example.com"
  description = "The domain designated for this VPC. Used for the VPC resource names and for DHCP within the VPC."
  type        = string
}

variable "enable_ipv6" {
  default     = false
  description = "When true, enable IPv6 routing in this VPC."
  type        = bool
}

variable "nat_redundancy" {
  default     = 3
  description = "Determines the maximum number of NAT gateways to be created, minimum of 1, regardless of how many availability zones are used."
  type        = number
}

variable "tags" {
  default     = {}
  description = "Tags to be set on all resources provisioned by this module."
  type        = map(string)
}
