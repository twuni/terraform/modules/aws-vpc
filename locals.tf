locals {
  nat_redundancy  = min(local.partition_count, var.nat_redundancy)
  partition_count = max(1, min(3, length(var.availability_zone_ids)))
}
