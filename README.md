# VPC | AWS | Terraform Modules | Twuni

This Terraform module provisions a VPC on AWS and a set of core
resources needed to successfully route traffic within the VPC.

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0.8 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.60.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_additional_subnet_tags"></a> [additional\_subnet\_tags](#input\_additional\_subnet\_tags) | Additional tags to be set on subnets provisioned by this module. Typically used for subnet auto-discovery such as with an AWS load balancer controller on EKS. | `map(string)` | `{}` | no |
| <a name="input_availability_zone_ids"></a> [availability\_zone\_ids](#input\_availability\_zone\_ids) | A list of zone IDs for AWS availability zones in which to provision subnets for this VPC's NAT gateway. | `list(string)` | `[]` | no |
| <a name="input_cidr_block"></a> [cidr\_block](#input\_cidr\_block) | The root CIDR block for this VPC. | `string` | `"10.0.0.0/16"` | no |
| <a name="input_domain"></a> [domain](#input\_domain) | The domain designated for this VPC. Used for the VPC resource names and for DHCP within the VPC. | `string` | `"example.com"` | no |
| <a name="input_enable_ipv6"></a> [enable\_ipv6](#input\_enable\_ipv6) | When true, enable IPv6 routing in this VPC. | `bool` | `false` | no |
| <a name="input_nat_redundancy"></a> [nat\_redundancy](#input\_nat\_redundancy) | Determines the maximum number of NAT gateways to be created, minimum of 1, regardless of how many availability zones are used. | `number` | `3` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Tags to be set on all resources provisioned by this module. | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_public_subnet_ids"></a> [public\_subnet\_ids](#output\_public\_subnet\_ids) | The IDs of subnets with direct access to the Internet. |
| <a name="output_trusted_subnet_ids"></a> [trusted\_subnet\_ids](#output\_trusted\_subnet\_ids) | The IDs of subnets behind a NAT gateway. |
| <a name="output_vpc_id"></a> [vpc\_id](#output\_vpc\_id) | The ID of this VPC. |
