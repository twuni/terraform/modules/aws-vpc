output "public_subnet_ids" {
  description = "The IDs of subnets with direct access to the Internet."
  value       = aws_subnet.public.*.id
}

output "trusted_subnet_ids" {
  description = "The IDs of subnets behind a NAT gateway."
  value       = aws_subnet.trusted.*.id
}

output "vpc_id" {
  description = "The ID of this VPC."
  value       = aws_vpc.root.id
}
