resource "aws_vpc" "root" {
  assign_generated_ipv6_cidr_block = var.enable_ipv6
  cidr_block                       = var.cidr_block

  tags = merge(var.tags, {
    Name = var.domain
  })
}

resource "aws_internet_gateway" "default" {
  vpc_id = aws_vpc.root.id

  tags = merge(var.tags, {
    Name = var.domain
  })
}

resource "aws_egress_only_internet_gateway" "default" {
  vpc_id = aws_vpc.root.id

  tags = merge(var.tags, {
    Name = var.domain
  })
}

resource "aws_nat_gateway" "trusted" {
  count         = local.nat_redundancy
  allocation_id = aws_eip.nat[count.index].id
  subnet_id     = aws_subnet.public[count.index].id

  tags = merge(var.tags, {
    Name = "${var.domain}-${var.availability_zone_ids[count.index]}"
  })

  depends_on = [aws_internet_gateway.default]
}

resource "aws_eip" "nat" {
  count = local.nat_redundancy
  vpc   = true

  tags = merge(var.tags, {
    Name = "${var.domain}-nat-${var.availability_zone_ids[count.index]}"
  })
}

resource "aws_subnet" "trusted" {
  count                           = local.partition_count
  assign_ipv6_address_on_creation = var.enable_ipv6
  availability_zone_id            = var.availability_zone_ids[count.index]
  cidr_block                      = cidrsubnet(aws_vpc.root.cidr_block, 4, count.index)
  ipv6_cidr_block                 = var.enable_ipv6 ? cidrsubnet(aws_vpc.root.ipv6_cidr_block, 4, count.index) : null
  vpc_id                          = aws_vpc.root.id

  tags = merge(var.tags, var.additional_subnet_tags, {
    Name                              = "${var.domain}-trusted-${var.availability_zone_ids[count.index]}"
    "kubernetes.io/role/internal-elb" = "1"
  })
}

resource "aws_subnet" "public" {
  count                           = local.partition_count
  assign_ipv6_address_on_creation = var.enable_ipv6
  availability_zone_id            = var.availability_zone_ids[count.index]
  cidr_block                      = cidrsubnet(aws_vpc.root.cidr_block, 4, 8 + count.index)
  ipv6_cidr_block                 = var.enable_ipv6 ? cidrsubnet(aws_vpc.root.ipv6_cidr_block, 4, 8 + count.index) : null
  vpc_id                          = aws_vpc.root.id

  tags = merge(var.tags, var.additional_subnet_tags, {
    Name                     = "${var.domain}-public-${var.availability_zone_ids[count.index]}"
    "kubernetes.io/role/elb" = "1"
  })
}

resource "aws_route_table" "public" {
  count  = local.partition_count
  vpc_id = aws_vpc.root.id

  tags = merge(var.tags, {
    Name = "${var.domain}-public-${var.availability_zone_ids[count.index]}"
  })
}

resource "aws_route_table" "trusted" {
  count  = local.partition_count
  vpc_id = aws_vpc.root.id

  tags = merge(var.tags, {
    Name = "${var.domain}-trusted-${var.availability_zone_ids[count.index]}"
  })
}

resource "aws_route_table_association" "public" {
  count          = local.partition_count
  subnet_id      = aws_subnet.public[count.index].id
  route_table_id = aws_route_table.public[count.index].id
}

resource "aws_route_table_association" "trusted" {
  count          = local.partition_count
  subnet_id      = aws_subnet.trusted[count.index].id
  route_table_id = aws_route_table.trusted[count.index].id
}

resource "aws_route" "public-ipv4" {
  count                  = local.partition_count
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.default.id
  route_table_id         = aws_route_table.public[count.index].id
}

resource "aws_route" "public-ipv6" {
  count                       = var.enable_ipv6 ? local.partition_count : 0
  destination_ipv6_cidr_block = "::/0"
  gateway_id                  = aws_internet_gateway.default.id
  route_table_id              = aws_route_table.public[count.index].id
}

resource "aws_route" "trusted-ipv4" {
  count                  = local.partition_count
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.trusted[count.index % local.nat_redundancy].id
  route_table_id         = aws_route_table.trusted[count.index].id
}

resource "aws_route" "trusted-ipv6" {
  count                       = var.enable_ipv6 ? local.partition_count : 0
  destination_ipv6_cidr_block = "::/0"
  egress_only_gateway_id      = aws_egress_only_internet_gateway.default.id
  route_table_id              = aws_route_table.trusted[count.index].id
}

resource "aws_vpc_dhcp_options" "domain" {
  domain_name = var.domain

  domain_name_servers = [
    "AmazonProvidedDNS"
  ]

  tags = merge(var.tags, {
    Name = var.domain
  })
}

resource "aws_vpc_dhcp_options_association" "domain" {
  vpc_id          = aws_vpc.root.id
  dhcp_options_id = aws_vpc_dhcp_options.domain.id
}
